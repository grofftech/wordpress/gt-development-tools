<?php

/**
 * Kint Settings
 *
 * @package     GroffTech\GtDevelopmentTools\Support
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GPL-2.0+
 */

namespace GroffTech\GtDevelopmentTools\Support;

use Kint\Renderer\RichRenderer as KintRenderer;

/**
 * Kint Settings class.
 */
class KintSettings {

    /**
     * Setup Kint.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function setup() {
        KintRenderer::$folder = false;
    }
}
