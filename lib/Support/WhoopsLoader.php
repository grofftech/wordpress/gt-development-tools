<?php

/**
 * Whoops Loader.
 *
 * @package     GroffTech\GtDevelopmentTools\Support
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GPL-2.0+
 */

namespace GroffTech\GtDevelopmentTools\Support;

use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

/**
 * Whoops Loader class.
 */
class WhoopsLoader {
    /**
     * Setup Whoops.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function setup() {
        $whoops     = new Run();
        $error_page = new PrettyPageHandler();

        $error_page->setEditor('vscode');
        $whoops->pushHandler($error_page);
        $whoops->register();
    }
}
