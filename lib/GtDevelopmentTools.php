<?php
/**
 * Initialization
 *
 * @package     GroffTech\GtDevelopmentTools
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\GtDevelopmentTools;

use GroffTech\GtDevelopmentTools\Support\WhoopsLoader;
use GroffTech\GtDevelopmentTools\Support\KintSettings;

/**
 * Gt Development Tools class.
 */
class GtDevelopmentTools {

    /**
     * Constructor.
     */
    function __construct()
    {
        $this->init_constants();
        WhoopsLoader::setup();
        KintSettings::setup();
    }

    /**
     * Setup the plugin's constants.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function init_constants()
    {
        if ( ! defined( 'GT_DEVELOPMENT_TOOLS_URL' ) ) {
            define( 'GT_DEVELOPMENT_TOOLS_URL', plugin_dir_url( __FILE__ ) );
        }

        if ( ! defined( 'GT_DEVELOPMENT_TOOLS_DIR' ) ) {
            define( 'GT_DEVELOPMENT_TOOLS_DIR', plugin_dir_path( __FILE__ ) );
        }
    }
}
