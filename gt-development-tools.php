<?php

/**
 * GT Development Tools Plugin
 *
 * @package     GroffTech\GtDevelopment Tools
 * @author      Brett Groff
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name:  GT Development Tools
 * Plugin URI:   https://gitlab.com/grofftech/wordpress/gt-development-tools
 * Description:  A suite of tools for your WordPress development environment.
 * Version:      1.0.0
 * Author:       groffTECH
 * Author URI:   https://grofftech.net
 * Text Domain:  gt-development-tools
 * License:      GPL-2.0+
 * License URI:  http://www.gnu.org/licenses/gpl-2.0.txt
 * Requires PHP: 7.4
 */

namespace GroffTech\GtDevelopmentTools;

if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Not a valid WordPress Installation!' );
}

require_once( __DIR__ . '/vendor/autoload.php' );
$gt_development_tools = new GtDevelopmentTools();